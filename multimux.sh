#!/bin/bash

SESSIONNAME="misterupdate"
ssh_user="root"
ssh_list=("mister.tv.local" "mister.office.local")
session_list=()

spawn_detached_window() {
   local session="$1"
   local user="$2"
   local ssh_target="$3"

   tmux new-session -d -s "${session}" \; \
   rename-window "${session}" \; \
   send-keys "ssh ${user}@${ssh_target}" C-m
}

# set up detached sessions
for mister_node in "${ssh_list[@]}"; do
  session="mister-$(openssl rand -hex 2)"
  session_list+=("$session")

  spawn_detached_window "$session" "$ssh_user" "$mister_node"
done

# Wait for SSH connections to establish
sleep 6

# Create main session and setup panes
tmux new-session -d -s "$SESSIONNAME" -n main
tmux select-window -t "$SESSIONNAME":0

# Now, instead of assuming the number of panes, we'll dynamically create and attach sessions
for i in "${!session_list[@]}"; do
  if [ $i -eq 0 ]; then
    tmux send-keys -t "$SESSIONNAME" "unset TMUX; tmux attach-session -t ${session_list[$i]}" C-m
  else
    tmux split-window -h -t "$SESSIONNAME" # This line ensures vertical pane splitting
    tmux send-keys -t "$SESSIONNAME.$i" "unset TMUX; tmux attach-session -t ${session_list[$i]}" C-m
  fi
  tmux select-layout -t "$SESSIONNAME" tiled
done

# Synchronized commands go here: 

# Enable synchronized input across all panes
tmux setw -t "$SESSIONNAME" synchronize-panes on

# Send 'doupdate' command to all panes
tmux send-keys -t "$SESSIONNAME" "doupdate" C-m

# Finally, attach to the main session
tmux attach -t "$SESSIONNAME"
